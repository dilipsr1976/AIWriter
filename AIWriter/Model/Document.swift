//
//  Email.swift
//  AIWriter
//
//  rebootcs.com
//

import Foundation
import RealmSwift
class Document: Object {
    @Persisted var subject: String = ""
    @Persisted var body: String = ""
    @Persisted var date: Date = Date()
    
    convenience init(subject: String, body: String, date: Date) {
        // Call the superclass initializer.
        self.init()
        // Set the values of the properties `subject`, `body`, and `date`.
        self.subject = subject
        self.body = body
        self.date = date
    }
}
