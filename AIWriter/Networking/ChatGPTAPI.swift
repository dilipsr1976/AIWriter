//
//  ChatGPTAPI.swift
//  AIWriter
//
//  rebootcs.com
//

import Foundation



class ChatGPTAPI {
    
    // Singleton instance
    static let shared = ChatGPTAPI()
    
    // Method for making a request to the OpenAI chat API
    func request(with messages: [[String: Any]], completionHandler: @escaping (Result<String, Error>) -> Void) {
        
        // Define the URL for the API
        let url = URL(string: "https://api.openai.com/v1/chat/completions")!
        
        // Create a URLRequest with the URL
        var request = URLRequest(url: url)
        
        // Set the HTTP method of the request to POST
        request.httpMethod = "POST"
        
        // Set the Content-Type header to application/json
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Set the Authorization header to include the OpenAI token
        request.setValue("Bearer \(AppConfig.openAIToken)", forHTTPHeaderField: "Authorization")
        
        // Define the body of the request as a dictionary with the model name and the messages to send
        let body: [String: Any] = [
            "model": "gpt-3.5-turbo",
            "messages": messages
        ]
        
        // Serialize the body as JSON data
        let data = try! JSONSerialization.data(withJSONObject: body, options: [])
        print(data)
        
        // Create a URLSession task to make the request
        let task = URLSession.shared.uploadTask(with: request, from: data) { data, response, error in
            
            // Handle any errors that occur during the request
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            
            // Check if there is any data returned from the API
            guard let data = data else {
                completionHandler(.failure(NSError(domain: "com.yourapp", code: 1, userInfo: [NSLocalizedDescriptionKey: "Empty response data"])))
                return
            }
            
            // Deserialize the JSON data into a dictionary
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
                // Extract the "choices" array from the dictionary and take the first element
                let choices = json?["choices"] as? [[String: Any]]
                let firstChoice = choices?.first
                
                // Extract the "message" dictionary from the first element and get the "content" string
                let message = firstChoice?["message"] as? [String: Any]
                let content = message?["content"] as? String ?? ""
                
                // Call the completion handler with the content string
                completionHandler(.success(content))
            } catch {
                // Call the completion handler with any errors that occur during deserialization
                completionHandler(.failure(error))
            }
        }
        
        // Start the URLSession task
        task.resume()
    }
}
