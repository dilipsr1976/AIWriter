//
//  SettingsTableViewController.swift
//  Static Table View
//
//  rebootcs.com
//

import MessageUI
import StoreKit
import UIKit

class SettingsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    // IBOutlet to connect the table view with the view controller
    @IBOutlet var settingsTableView: UITableView!
    
    // Method that gets called after the view has been loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the table view's delegate to this view controller
        settingsTableView.delegate = self
    }
    
    // Method that gets called when the "Upgrade to Premium" button is clicked
    func onUpgradePremiumButtonClicked() {
        // Check if user is not already subscribed
        if !SubscriptionManager.shared.isSubscribed() {
            // Instantiate the subscription view controller
            let subscriptionController = self.storyboard!.instantiateViewController(
                identifier: "subscription")
            subscriptionController.modalPresentationStyle = .fullScreen
            // Present the subscription view controller
            self.present(subscriptionController, animated: false)
        } else {
            // User is already subscribed, show alert
            let alert = UIAlertController(
                title: "You are already subscribed to Dissemino Ai Writer Pro", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    // Method that gets called when the "Restore Purchase" button is clicked
    func onRestorePremiumButtonClicked() {
        // Check if user is not already subscribed
        if !SubscriptionManager.shared.isSubscribed() {
            // Call the restore transaction method in the SubscriptionManager
            SubscriptionManager.shared.restoreTransaction()
        } else {
            // User is already subscribed, show alert
            let alert = UIAlertController(
                title: "You are already subscribed to Dissemino Ai Writer Pro", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    // Function called when a table view cell is selected
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Instantiate the Main storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        // Use a switch statement to determine which section and row of the table view was selected
        switch indexPath.section {
            
        case 0:
            // For section 0, determine which row was selected
            switch indexPath.row {
            case 0:
                // Call the onUpgradePremiumButtonClicked() function
                onUpgradePremiumButtonClicked()
                
            case 1:
                // Call the onRestorePremiumButtonClicked() function
                onRestorePremiumButtonClicked()
                
            default:
                break
            }
            
        case 1:
            // For section 1, determine which row was selected
            switch indexPath.row {
            case 0:
                // Print 11 and request a review for the app
                print(11)
                SKStoreReviewController.requestReview()
                
            case 1:
                // Print 12 and create an activity view controller to share the app URL and text
                print(12)
                let url = URL(string: AppConfig.appURL)!
                let activityViewController = UIActivityViewController(
                    activityItems: [AppConfig.shareAppText, url], applicationActivities: nil)
                
                // Present the activity view controller
                present(activityViewController, animated: true, completion: nil)
                
            default:
                break
            }
            
        case 2:
            // For section 2, determine which row was selected
            switch indexPath.row {
            case 0:
                // Print 21 and create a mail composer to send an email
                print(21)
                if MFMailComposeViewController.canSendMail() {
                    let mailComposer = MFMailComposeViewController()
                    mailComposer.mailComposeDelegate = self
                    mailComposer.setToRecipients([AppConfig.workEmail])
                    mailComposer.setSubject(AppConfig.documentSubject)
                    mailComposer.setMessageBody("", isHTML: false)
                    present(mailComposer, animated: true, completion: nil)
                } else {
                    // If email cannot be sent, print a message
                    print("Cannot send email")
                }
            case 1:
                // Print 22 and open the app's privacy policy in the browser
                print(22)
                if let url = URL(string: AppConfig.privacyPolicy) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            case 2:
                // Print 23 and open the app's terms of use in the browser
                print(23)
                if let url = URL(string: AppConfig.termsOfUse) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            default:
                break
            }
        default:
            break
        }
        // Deselect the row that was selected
        if let indexPathForSelectedRow = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathForSelectedRow, animated: true)
        }
    }

    // Function called when the user finishes composing an email
    func mailComposeController(
        _ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult,
        error: Error?
    ) {
        // Dismiss the mail composer
        controller.dismiss(animated: true, completion: nil)
    }

}
