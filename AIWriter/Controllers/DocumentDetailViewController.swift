import UIKit
import MessageUI
import GoogleMobileAds

class DocumentDetailViewController: UIViewController,MFMailComposeViewControllerDelegate, GADFullScreenContentDelegate{
    
    // Declare a public variable named "document" of type Document
    public var document: Document!
    
    // Declare a private variable named "interstitial" of type GADInterstitialAd
    private var interstitial: GADInterstitialAd?
    
    // Declare an IBOutlet named "content" that is a weak reference to a UITextView
    @IBOutlet weak var content: UITextView!
    
    // Declare an IBAction named "dismissController" that dismisses the current view controller
    @IBAction func dismissController(){
        self.dismiss(animated: false)
    }
    
    // This method is called when the view is about to appear on screen
    override func viewWillAppear(_ animated: Bool) {
        // Set the text of the UITextView named "content" to display the subject and body of the document
        self.content.text = "Subject: \(document.subject )\n\n\(document.body)"
    }
    
    // This method is called after the view has loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if the user is subscribed, and if not, load a Google Ad
        if !SubscriptionManager.shared.isSubscribed() {
            loadGoogleAd()
        }
        
        // Create a UITapGestureRecognizer to dismiss the keyboard when the user taps outside of the keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // This method loads a Google Ad
    func loadGoogleAd(){
        // Create a GADRequest object
        let request = GADRequest()
        // Load an interstitial ad with the given ad unit ID and request, and handle the result in a completion handler
        GADInterstitialAd.load(withAdUnitID: AppConfig.adMobId,
                               request: request,
                               completionHandler: { [self] ad, error in
            if let error = error {
                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                return
            }
            // If the ad loaded successfully, set the "interstitial" variable to the loaded ad and set its fullScreenContentDelegate to self
            interstitial = ad
            interstitial?.fullScreenContentDelegate = self
            
            // If the "interstitial" variable is not nil, present the ad from the root view controller
            if interstitial != nil {
                interstitial?.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }
        )
    }
    
    // The following three methods are delegate methods for the GADInterstitialAd's fullScreenContentDelegate
    // They are called when an interstitial ad is presented, fails to present, or is dismissed
    
    /// Tells the delegate that the ad failed to present full screen content.
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("Ad did fail to present full screen content.")
    }
    
    /// Tells the delegate that the ad will present full screen content.
    func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad will present full screen content.")
    }
    
    /// Tells the delegate that the ad dismissed full screen content.
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad did dismiss full screen content.")
    }
    
    // This function is called when the user taps the "Share" button
    @IBAction func shareDocument(_ sender: Any) {
        // If there is text in the "content" text view, create a UIActivityViewController and present it
        if let text = content.text {
            let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
            present(activityViewController, animated: true, completion: nil)
        }
    }
    // This function is called when the user taps the "Copy" button
    @IBAction func copyDocument(_ sender: Any) {
        // If there is text in the "content" text view, copy it to the clipboard and display an alert
        if let text = content.text {
            UIPasteboard.general.string = text
            let alert = UIAlertController(title: "Text Copied", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    // This function is called when the user taps outside of the keyboard area
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}

