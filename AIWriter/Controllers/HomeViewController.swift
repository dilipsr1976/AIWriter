//
//  ViewController.swift
//  AI Writer
//
//  rebootcs.com
//

import UIKit

class HomeViewController: UIViewController, UITextViewDelegate {
    
    
    
    // Outlets for the different UI components
    @IBOutlet weak var proLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tone: UISlider!
    @IBOutlet weak var length: UISlider!
    @IBOutlet weak var instruction: UITextView!
    @IBOutlet weak var upgrade: UIButton!
    @IBOutlet weak var freeLimit: UILabel!
    @IBOutlet weak var lengthView: UIView!
    @IBOutlet weak var ToneVIew: UIView!
    @IBOutlet weak var lengthIndicator: UILabel!
    
    // Available options for lengths and tones
    var availableLengths = [100, 250, 500]
    var availableTones = ["Formal", "Neutral", "Friendly"]
    
    // Default text for the instruction text view
    var placeHolder = "Hi [Name], I wanted to let you know that I am unhappy with [Product/Service]. Can we discuss how to resolve this?"
    
    // Default length for the email
    var currentLength = 250
    
    // Delegate methods for the instruction text view
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Set up the UI components
        setupTextView()
        setupRoundedCorners()
        setupSliders()
        setupSubscriptionView()
    }
    
    // Set up the instruction text view with the default text and color
    private func setupTextView() {
        instruction.delegate = self
        instruction.text = Constants.placeHolderText
        instruction.textColor = UIColor.lightGray
    }
    
    // Set up rounded corners for the views
    private func setupRoundedCorners() {
        DispatchQueue.main.async {
            self.instruction.addRoundedCorners(with: Constants.cornerRadius)
            self.lengthView.addRoundedCorners(with: Constants.cornerRadius)
            self.ToneVIew.addRoundedCorners(with: Constants.cornerRadius)
        }
    }
    
    // Sets up the tone and length sliders as non-continuous
    private func setupSliders() {
        tone.isContinuous = false
        length.isContinuous = false
    }
    
    // Sets up the subscription view based on the user's subscription status and updates UI elements accordingly
    private func setupSubscriptionView() {
        if SubscriptionManager.shared.isSubscribed() {
            // If user is subscribed, hide upgrade button, free limit label and show the "Pro" label
            DispatchQueue.main.async {
                self.upgrade.isHidden = true
                self.freeLimit.isHidden = true
                self.proLabel.isHidden = false
            }
        } else {
            // If user is not subscribed, show upgrade button, free limit label and hide the "Pro" label
            // Also, update the free limit label with the daily usage left for the user
            DispatchQueue.main.async {
                self.upgrade.isHidden = false
                self.freeLimit.isHidden = false
                self.proLabel.isHidden = true
                self.freeLimit.text = "\(LimitFunctionality.shared.getDailyUsageLeft()) \(Constants.freeRequestsLeftText)"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Sets up a tap gesture recognizer to dismiss the keyboard when the user taps outside the keyboard area
        let tap = UITapGestureRecognizer(
            target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // Dismisses the keyboard by resigning the first responder status of the view
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // Presents the subscription screen when the upgrade button is clicked
    @IBAction func onUpgradeButtonClicked(_sender: Any) {
        let subscriptionController = self.storyboard!.instantiateViewController(
            identifier: "subscription")
        subscriptionController.modalPresentationStyle = .fullScreen
        self.present(subscriptionController, animated: false)
    }
    
    
    @IBAction func onSubmitClicked(_ sender: Any) {
        // Check if the user has exceeded the free usage limit
        
        if !SubscriptionManager.shared.isSubscribed() && !LimitFunctionality.shared.isFreeAllowed() {
            // If the limit has been exceeded, show subscription screen
            let subscriptionController = self.storyboard!.instantiateViewController(
                identifier: "subscription")
            subscriptionController.modalPresentationStyle = .fullScreen
            self.present(subscriptionController, animated: false)
            return
        }
        
        // Start the activity indicator to indicate loading
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            let button = sender as! UIButton
            button.isUserInteractionEnabled = false
        }
        
        // Prepare the prompt to send to OpenAI
        let currentTone = getTone()
        let prompt =
        "Write an article in a \(currentTone) tone and write at least \(currentLength) words. Respond with \"Subject:\" and \"Body:\" :\n \(instruction!.text!)."
        print(prompt)
        
        
        var messages = [["role" : "user", "content" : prompt]]
        
        ChatGPTAPI.shared.request(with: messages) { result in
            
            DispatchQueue.main.async {
                self.instruction.text = ""
                let button = sender as! UIButton
                button.isUserInteractionEnabled = true
            }
            
            switch result {
            case .success(let response):
                if !SubscriptionManager.shared.isSubscribed() {
                    LimitFunctionality.shared.incrementUsage()
                    
                }
                // Stop the activity indicator after the response has been handled
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
                
                self.handleDocumentResponse(message: response)
            case .failure(let error):
                print(error.localizedDescription)
                self.alertBox(message: "Servers Busy. Please try again.")
                self.activityIndicator.stopAnimating()
            }
            
            
        }}
    func handleDocumentResponse(message: String) {
        do {
            // Extract the body of the document from the response message
            let body = DocumentManager.shared.extractBody(from: message)
            
            // Extract the subject from the response message using a regular expression
            let subjectRegex = try! NSRegularExpression(
                pattern: "^Subject:(.+)$", options: .anchorsMatchLines)
            let subjectRange = NSRange(message.startIndex..., in: message)
            let subjectMatch = subjectRegex.firstMatch(in: message, options: [], range: subjectRange)
            var subject = subjectMatch.map {
                String(message[Range($0.range(at: 1), in: message)!])
            }
            if subject == nil {
                subject = ""
            }
            
            // Create a document object using the extracted subject and body, and the current date
            let document = Document(subject: subject!, body: body, date: Date())
            
            // Store the document and present it in a detail view controller
            DispatchQueue.main.async {
                DocumentManager.shared.storeDocument(subject: document.subject, body: document.body, date: document.date)
                let controller =
                self.storyboard!.instantiateViewController(identifier: "document")
                as! DocumentDetailViewController
                controller.document = document
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: false)
            }
        }
        catch {
            // Handle any errors that occur during document response handling
            print("Error decoding document response: \(error)")
        }
    }

    @IBAction func slideToNearest(_ sender: UISlider) {
        // Round the slider value to the nearest integer and update the slider value
        DispatchQueue.main.async {
            let roundedValue = sender.value.rounded()
            sender.value = Float(Int(roundedValue))
            
            // Update the current length if the slider is for length, and the length indicator label
            let n = Int(roundedValue)
            if sender.tag == 0 {
                self.currentLength = self.availableLengths[n - 1]
                DispatchQueue.main.async {
                    self.lengthIndicator.text = "upto \(self.currentLength) words"
                }
            }
        }
    }

    func getTone() -> String {
        // Get the current tone by rounding the tone slider value to the nearest integer and returning the tone at that index in the availableTones array
        let n = Int(tone.value.rounded())
        return availableTones[n - 1]
    }
}



// An extension for UIView to add rounded corners to a view
extension UIView {
    func addRoundedCorners(with radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}

// An extension for UIViewController to display an alert box with a message
extension UIViewController {
    func alertBox(message: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// A struct to hold constant values used throughout the app
struct Constants {
    static let placeHolderText = "Enter text here..." // Default text for text fields
    static let cornerRadius: CGFloat = 10.0 // Default corner radius for views with rounded corners
    static let freeRequestsLeftText = "FREE request left" // Text to display for remaining free requests
}

