// Import necessary frameworks
import UIKit
import RealmSwift
import MessageUI


class MyDocumentsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,MFMailComposeViewControllerDelegate, UICollectionViewDelegateFlowLayout {
    
    // Connect UI elements with code and define class properties
    @IBOutlet var collectionView: UICollectionView!
    var documents: Results<Document>?
    @IBOutlet weak var proLabel: UILabel!
    var notificationToken: NotificationToken?
    @IBOutlet weak var recentLabel: UILabel!
    @IBOutlet weak var upgrade: UIButton!
    
    @IBOutlet weak var freeLimit: UILabel!
    
    // Set up the subscription view by hiding/showing certain UI elements depending on whether the user is subscribed
    private func setupSubscriptionView() {
        if SubscriptionManager.shared.isSubscribed() {
            DispatchQueue.main.async {
                self.upgrade.isHidden = true
                self.freeLimit.isHidden = true
                self.proLabel.isHidden = false
            }
        } else {
            DispatchQueue.main.async {
                self.upgrade.isHidden = false
                self.freeLimit.isHidden = false
                self.proLabel.isHidden = true
                self.freeLimit.text = "\(LimitFunctionality.shared.getDailyUsageLeft()) \(Constants.freeRequestsLeftText)"
            }
        }
    }
    // Update the UI
    func setupUI(){
        
        setupSubscriptionView()
        // If there are no documents, show the emptyState view and hide the recentLabel and collectionView
        if documents?.count == 0 {
            DispatchQueue.main.async {
                self.recentLabel.isHidden = true
            }
        } else {
            // If there are documents, show the recentLabel and collectionView and hide the emptyState view
            recentLabel.isHidden = false
            
        }
    }
    
    // Reload the data every time the view appears
    override func viewWillAppear(_ animated: Bool) {
        // Retrieve the documents from the database using the DocumentManager
        DispatchQueue.main.async {
            self.documents = DocumentManager.shared.retrieveDocuments()
            self.setupUI()
            self.collectionView.reloadData()
        }
        
    }
    
    
    
    // Set up the notification token to update the collection view when the data changes
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var ap = [1,2,3]
        //var t = ap[4]
        notificationToken = documents?.observe { [weak self] changes in
            guard let collectionView = self?.collectionView else { return }
            switch changes {
            case .initial:
                collectionView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                collectionView.performBatchUpdates({
                    collectionView.insertItems(at: insertions.map({ IndexPath(row: $0, section: 0) }))
                    collectionView.deleteItems(at: deletions.map({ IndexPath(row: $0, section: 0)}))
                    collectionView.reloadItems(at: modifications.map({ IndexPath(row: $0, section: 0) }))
                }, completion: nil)
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
        
    }
    
    
    
    // Invalidate the notification token
    deinit {
        notificationToken?.invalidate()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    
    // Set the number of items in the collection view to the number of documents
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let documents = documents {
            return documents.count + 1
        }
        return  1
    }
    
    // This function is called to configure the cell for the corresponding index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreatorCell", for: indexPath)
            cell.addRoundedCorners(with: 5.0)
            cell.addDashedBorder()
            
            return cell
            
            
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WriterCell", for: indexPath) as! WriterCell
        
        // Retrieve the document object for the current index path
        let document = documents![indexPath.row - 1]
        
        // Set the cell's title, body, and date labels to the corresponding document properties
        cell.titleLabel.text = document.subject
        cell.bodyLabel.text = document.body
        cell.dateLabel.text = document.date.description
        
        // Add a target and tag to the cell's "more" button for actions
        cell.more.addTarget(self, action: #selector(showActionsForSelectedDocument(_:)), for: .touchUpInside)
        cell.more.tag = indexPath.row
        
        // Add rounded corners to the cell
        cell.addRoundedCorners(with: 5.0)
        
        // Return the configured cell
        return cell
    }
    
    // This function is called after the view has been laid out
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Resize the collection view to fit the view's bounds
        // collectionView.frame = view.bounds
    }
    
    // This function is called when a cell is selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
            
            if !SubscriptionManager.shared.isSubscribed() && !LimitFunctionality.shared.isFreeAllowed() {
                // If the limit has been exceeded, show subscription screen
                let subscriptionController = self.storyboard!.instantiateViewController(
                    identifier: "subscription")
                subscriptionController.modalPresentationStyle = .fullScreen
                self.present(subscriptionController, animated: false)
                return
            }
            
            else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                var nextVC = storyboard.instantiateViewController(withIdentifier: "tab") as! UITabBarController
                nextVC.selectedIndex = 1
                navigationController?.pushViewController(nextVC, animated: true)
                nextVC.modalPresentationStyle = .fullScreen
                present(nextVC, animated: true, completion: nil)
                
                
                return
                
            }
        }
        // Create a new instance of the DocumentDetailViewController
        let controller = self.storyboard!.instantiateViewController(identifier: "document") as! DocumentDetailViewController
        
        // Set the controller's document property to the corresponding document object
        controller.document = documents![indexPath.row - 1]
        
        // Set the presentation style to full screen
        controller.modalPresentationStyle = .fullScreen
        
        // Present the controller
        self.present(controller, animated: false)
    }
    
    // This function shows an action sheet for the selected document
    func showActions(for document: Document) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let documentAsText = "Subject: \(document.subject)\n\nBody:\(document.body)"
        
        
        let shareAction = UIAlertAction(title: "Share", style: .default) { [weak self] _ in
            self?.shareDocument(document: documentAsText)
        }
        alertController.addAction(shareAction)
        
        let copyAction = UIAlertAction(title: "Copy", style: .default) { [weak self] _ in
            self?.copyDocument(document: documentAsText)
        }
        alertController.addAction(copyAction)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { [weak self] _ in
            self?.deleteDocument(document: document)
        }
        alertController.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    // Show action sheet for selected document
    @objc func showActionsForSelectedDocument(_ sender: UIButton) {
        let document = documents![sender.tag - 1]
        showActions(for: document)
    }
    
    // Share the document by opening the iOS share sheet
    @IBAction  func shareDocument(document: String) {
        let activityViewController = UIActivityViewController(activityItems: [document], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    // Copy the document to the clipboard and show an alert
    @IBAction func copyDocument(document: String) {
        UIPasteboard.general.string = document
        let alert = UIAlertController(title: "Document Copied", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    // Delete the document from the Realm database and show a confirmation alert
    func deleteDocument(document: Document) {
        // Use Realm to delete the document object
        let docRef = ThreadSafeReference(to: document)
        DocumentManager.shared.deleteDocument(document: docRef)
        documents = DocumentManager.shared.retrieveDocuments()
        collectionView.reloadData()
        // Show a confirmation alert
        let alert = UIAlertController(title: "Document Deleted", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        setupUI()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func onUpgradeButtonClicked(_sender: Any) {
        
        
        let subscriptionController = self.storyboard!.instantiateViewController(
            identifier: "subscription")
        subscriptionController.modalPresentationStyle = .fullScreen
        self.present(subscriptionController, animated: false)
    }
    
    
}
extension UIView {
    func addDashedBorder() {
        let color = UIColor.darkGray.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}
