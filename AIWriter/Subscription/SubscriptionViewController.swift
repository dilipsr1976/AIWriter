//
//  ViewController.swift
//  subscription
//
//  rebootcs.com
//

import UIKit
import ZKCarousel
import StoreKit
import SwiftyStoreKit
class SubscriptionViewController: UIViewController,SubscriptionManagerDelegate {
    
    @IBOutlet weak var offerDescription: UILabel!
    @IBOutlet weak var offerTitle: UILabel!
    @IBOutlet weak var policy: UILabel!
    @IBOutlet weak var susbcriptionViewGroup: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var refresh: UIButton!
    @IBOutlet weak var subscriptionButton: UIButton!
    
    @IBOutlet weak var subscriptionGif: UIImageView!
    
    var products: [SKProduct]?
    func onProductDetailsFetched(products: [SKProduct]) {
        
        stopLoadingAnimation()
        
        if products.count == 0 {
            DispatchQueue.main.async {
                SweetAlert().showAlert("Error", subTitle: "Unable to find any product with the provided identifier", style: .error, buttonTitle: "ok")
            }
            return
        }
        
        self.products = products
        var isProductFound = false
        for product in products {
            if product.productIdentifier == SubscriptionManager.shared.productIdentifier {
                
                print(product)
                
                isProductFound = true
                
                
                
                if SubscriptionManager.shared.isSubscription {
                    if let period = product.introductoryPrice?.subscriptionPeriod {
                        DispatchQueue.main.async {
                            print(product)
                            
                            
                            self.offerDescription.text = "\(period.numberOfUnits) \(self.unitName(unitRawValue: period.unit.rawValue)) FREE trial, then \(product.priceLocale.currencySymbol!) \(product.price.floatValue) / \(product.subscriptionPeriod!.numberOfUnits) \(self.unitName(unitRawValue: product.subscriptionPeriod!.unit.rawValue))"
                        }
                    }
                    else {
                        
                        DispatchQueue.main.async {
                            self.offerDescription.text = "\(product.priceLocale.currencySymbol!) \(product.price.floatValue) / \(product.subscriptionPeriod!.numberOfUnits) \(self.unitName(unitRawValue: product.subscriptionPeriod!.unit.rawValue))"
                        }
                        
                    }
                    
                    
                }
                else {
                    
                    DispatchQueue.main.async {
                        self.offerDescription.text = "\(product.priceLocale.currencySymbol!) \(product.price.floatValue)"
                    }
                    
                }
            }
        }
        stopLoadingAnimation()
        
        if isProductFound {
            DispatchQueue.main.async {
                self.susbcriptionViewGroup.isHidden = false
            }
        }
        
    }
    
    func onProductDetailsFetchedError(error: Error) {
        DispatchQueue.main.async {
            self.stopLoadingAnimation()
            
            SweetAlert().showAlert("Error", subTitle: "\(error.localizedDescription)", style: .error, buttonTitle: "Ok")
            
        }
        
    }
    @IBAction func closeSubscription(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func refresh(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            SweetAlert().showAlert("Error", subTitle: "Please check your internet connection", style: .error, buttonTitle: "Ok")
            return
        }
        
        else {
            fetchProducts()
        }
        
    }
    
    func setupViews(){
        
        DispatchQueue.main.async {
            self.offerTitle.text = SubscriptionManager.shared.title
            self.policy.text = SubscriptionManager.shared.policy
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        setupViews()
        

        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      
        
        var timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: true, block: { [weak self] (timer) in
            guard let self = self else { return }
 
            UIView.animate(withDuration: 0.3 , delay:0, options: [.allowUserInteraction], animations: {
                self.subscriptionButton.isUserInteractionEnabled = true
                self.subscriptionButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            },completion: { (finished) in
                
                UIView.animate(withDuration: 0.3, delay:0,  options:[.allowUserInteraction], animations: {
                    self.subscriptionButton.isUserInteractionEnabled = true

                    self.subscriptionButton.transform = CGAffineTransform.identity
                })
            })
            
        })
        
        
        do {
            let gif =  try UIImage(gifName: "subscription.gif")
            self.subscriptionGif.setGifImage(gif, loopCount: -1) // Will loop forever
        }
        catch {
            
        }

        
        
        registerNotifications()
        SubscriptionManager.shared.delegate = self
        fetchProducts()
        
    }
    
    func fetchProducts()  {
        
        if !Reachability.isConnectedToNetwork() {
            
            DispatchQueue.main.async {
                self.refresh.isHidden = false
                self.offerDescription.text = "Failed to load offer."
            }
        }
        
        else {
            
            DispatchQueue.main.async {
                self.refresh.isHidden = true
            }
            startLoadingAnimation()
            SubscriptionManager.shared.fetchProducts(matchingIdentifiers: [SubscriptionManager.shared.productIdentifier])
            
        }    }
    
    
    func startLoadingAnimation(){
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    func stopLoadingAnimation(){
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    
    @objc private func onSubscriptionFailed(notification: NSNotification){
        DispatchQueue.main.async {
            self.subscriptionButton.isUserInteractionEnabled = true
        }
        stopLoadingAnimation()
        if let error = notification.object as? Error {
            var controller = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            var alert = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            controller.addAction(alert)
            DispatchQueue.main.async {
                self.present(controller, animated: false, completion: nil)
            }
        }
        
    }
    
    
    @objc private func onSubscriptionExpired(notification: NSNotification){
        DispatchQueue.main.async {
            self.subscriptionButton.isUserInteractionEnabled = true
        }
        stopLoadingAnimation()
        SweetAlert().showAlert("Error", subTitle: "Your subscription has expired", style: .error, buttonTitle: "OK")
    }
    
    @objc private func onSubscriptionPurchased(notification: NSNotification){
        stopLoadingAnimation()
 
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @objc private func onSubscriptionNeverPurchased(notification: NSNotification){
        stopLoadingAnimation()
    }
    
    
    @objc private func onSubscriptionPurshasing(notification: NSNotification){
        
        startLoadingAnimation()
        print("called")
        //do stuff using the userInfo property of the notification object
    }
    
    
    @IBAction func onRestoreBtnClicked(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            SweetAlert().showAlert("Error", subTitle: "Please check your internet connection", style: .error, buttonTitle: "Ok")
            return
        }
        
        SubscriptionManager.shared.restoreTransaction()
        startLoadingAnimation()
        
        
    }
    
    
    @IBAction func onSubscribedBtnClicked(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.subscriptionButton.isUserInteractionEnabled = false
        }
        
        
        if !Reachability.isConnectedToNetwork() {
            SweetAlert().showAlert("Error", subTitle: "Please check your internet connection", style: .error, buttonTitle: "Ok")
            return
        }
        
        
        
        
        
        if let products = products {
            if  products.count  > 0  {
                DispatchQueue.main.async {
                    self.activityIndicator.startAnimating()
                }
                SubscriptionManager.shared.buyProduct(products[0])
            }
        }
        else {
            
            
            self.refresh(refresh)
            
            return
        }
        
        
        
        
        
    }
    
    

    
    @IBAction func openTermsOfUse(){
        guard let url = URL(string: "https://github.com/trendyapptemplates/EmailGPT/raw/main/termsOfUse.html") else { return }
        UIApplication.shared.open(url)
        
    }
    
    
    @IBAction  func openPrivacyPolicy(){
        guard let url = URL(string: "https://github.com/trendyapptemplates/EmailGPT/raw/main/privacyPolicy.html") else { return }
        UIApplication.shared.open(url)
        
    }
    func registerNotifications(){
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onSubscriptionFailed),
            name: .subscriptionFailed,
            object: nil)
        
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onSubscriptionExpired),
            name: .subscriptionExpired,
            object: nil)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onSubscriptionPurshasing),
            name: .subscriptionPurshasing,
            object: nil)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onSubscriptionPurchased),
            name: .subscriptionPurchased,
            object: nil)
        
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onSubscriptionNeverPurchased),
            name: .subscriptionNeverPurchased,
            object: nil)
        
    }
    
    func unitName(unitRawValue:UInt) -> String {
        switch unitRawValue {
        case 0: return "days"
        case 1: return "week"
        case 2: return "month"
        case 3: return "year"
        default: return ""
        }
    }
}



