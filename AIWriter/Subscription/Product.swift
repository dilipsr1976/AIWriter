//
//  Product.swift
//  Barcode Membership
//
//  rebootcs.com
//

import Foundation

struct Product:Codable {
    var identifier:String?
    var title:String
    var description:String
    var price:Float
    var currencySymbol:String
}
