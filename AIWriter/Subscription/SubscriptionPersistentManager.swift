//
//  PersistentManager.swift
//  BHGFamily
//
//  rebootcs.com
//

import UIKit
import StoreKit

class SubscriptionPersistentManager: NSObject {
    
    static let shared = SubscriptionPersistentManager()
    
    let defaults = UserDefaults.standard
    
    private func get<T:Codable>(forKey:String)->T?{
        let decoder = JSONDecoder()
        if let savedItem = defaults.object(forKey: forKey) as? Data {
            if let loadedItem = try? decoder.decode(T.self, from: savedItem) {
                return loadedItem
            }
        }
        return nil
    }
    
    private func unset(forKey:String){
        defaults.removeObject(forKey:forKey)
    }
    
    
    private func set<T:Codable>(type:T,forKey:String)->Bool{
        do{
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(type) {
                defaults.set(encoded, forKey: forKey)
                return true
            }
            return false
        }
        
    }
    
    
    func append<T:Codable>(typeOf:T)->Bool{
        
        let className = String(describing: type(of: typeOf))
        print(className)
        
        return true
    }
    func checkArray(item : AnyObject) -> Bool {
        return item is Array<AnyObject>
    }
    
    
}


extension SubscriptionPersistentManager {
    
    func setCurrentSubscription(productId:String?)->Bool{
        if let product = productId {
            return set(type: product, forKey: "subscription")
        }
        else {
            unset(forKey: "subscription")
            return true
        }
        
    }
    
    func registerNoOfAppLaunch()->Bool{
        
        if let counter:Int = get(forKey: "appLaunchCounter"){
            return set(type: (counter+1), forKey: "appLaunchCounter")
            
        }
        
        return set(type:1, forKey: "appLaunchCounter")
        
    }
    
    func getNoOfAppLaunch()->Int?{
        return get(forKey: "appLaunchCounter")
    }
    
    
    
    func getCurrentSubscription()->String?{
        return get(forKey: "subscription")
    }
    
    
    
}
