//
//  StoreObserver.swift
//  Learning
//
//  rebootcs.com
//

import Foundation
import StoreKit
import SwiftyStoreKit
class StoreObserver: NSObject, SKPaymentTransactionObserver {
    
    //Initialize the store observer.
    override init() {
        super.init()
        
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        onPurchased(productId: SubscriptionManager.shared.productIdentifier)
        
    }
    
    
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        
        NotificationCenter.default.post(name: .subscriptionFailed, object: error)
        
        
    }
    func onPurchased(productId:String){
        
        
        
        if SubscriptionManager.shared.isSubscription {
            SubscriptionManager.shared.verifyReceipt(productId: productId)
        }
        else {
            
            
            
            SubscriptionPersistentManager.shared.setCurrentSubscription(productId: productId)
            
            NotificationCenter.default.post(name: .subscriptionPurchased, object: nil)
        }
        
    }
    
    //Observe transaction updates.
    func paymentQueue(_ queue: SKPaymentQueue,updatedTransactions transactions: [SKPaymentTransaction]) {
        //Handle transaction states here.
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                SKPaymentQueue.default().finishTransaction(transaction)
                
                onPurchased(productId: transaction.payment.productIdentifier)
                
                break
            case .failed:
                SKPaymentQueue.default().finishTransaction(transaction)
                
                fail(transaction: transaction)
                NotificationCenter.default.post(name: .subscriptionFailed, object: transaction.error)
                
                break
            case .restored:
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .deferred:
                break
            case .purchasing:
                NotificationCenter.default.post(name: .subscriptionPurshasing, object: nil)
                
                break
            }
            
            
        }
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as NSError?,
           let localizedDescription = transaction.error?.localizedDescription,
           transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }
        
        
    }
    
}



