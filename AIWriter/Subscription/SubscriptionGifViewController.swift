//
//  SubscriptionGifViewController.swift
//  AIWriter
//
//  rebootcs.com
//

import UIKit
import SwiftyGif
class SubscriptionGifViewController: UIViewController {

    @IBOutlet weak var subscriptionButton: UIButton!
    @IBOutlet weak var subscriptionGif: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: true, block: { [weak self] (timer) in
            guard let self = self else { return }
            UIView.animate(withDuration: 0.3, animations: {
                self.subscriptionButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.subscriptionButton.transform = CGAffineTransform.identity
                })
            })
        })
        
        
        do {
            let gif =  try UIImage(gifName: "subscription.gif")
            self.subscriptionGif.setGifImage(gif, loopCount: -1) // Will loop forever
        }
        catch {
            
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
