//
//  SubscriptionManager.swift
//  Barcode Membership
//
//  rebootcs.com
//

import Foundation
import StoreKit
import SwiftyStoreKit

class SubscriptionManager: NSObject, SKProductsRequestDelegate {
    
    let configurationName = "Subscription"
    var productIdentifier = ""
    var status = false
    var sliderImages:[String] = []
    var title = ""
    var policy = ""
    var isSubscription:Bool = false
    var productDescription = ""
    var secretId = ""
    var googleAdId = ""
    
    var delegate:SubscriptionManagerDelegate?
    let iapObserver = StoreObserver()
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        delegate?.onProductDetailsFetched(products: response.products)
        
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        delegate?.onProductDetailsFetchedError(error: error)
    }
    
    static var shared = SubscriptionManager()
    
    var isAuthorizedForPayments: Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    func fetchProducts(matchingIdentifiers identifiers: [String]) {
        let productIdentifiers = Set(identifiers)
        let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productRequest.delegate = self
        productRequest.start()
    }
    public func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    public func restoreTransaction(){
        SKPaymentQueue.default().restoreCompletedTransactions()
        
        
    }
    
    
    func verifyReceipt(productId:String){
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: SubscriptionManager.shared.secretId)
        
        
        SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: false) { result in
            switch result {
                
            case .success(let receipt):
                
                print(receipt)
                
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable,
                    productId:productId ,
                    inReceipt: receipt)
                
                
                switch purchaseResult {
                    
                    
                case .purchased(let expiryDate, let receiptItems):
                    
                    for receipt1 in receiptItems {
                        print(" \(receipt1.originalTransactionId)) , \(receipt1.purchaseDate) , \(receipt1.subscriptionExpirationDate!) , \(receipt1.transactionId)")
                    }
                    
                    SubscriptionPersistentManager.shared.setCurrentSubscription(productId: productId)
                    
                    NotificationCenter.default.post(name: .subscriptionPurchased, object: nil)
                    
                    
                case .expired(let expiryDate, let receiptItems):
                    
                    print("expired purchased receipts count : \(receiptItems.count)")
                    
                    for receipt1 in receiptItems {
                        print(" \(receipt1.originalTransactionId)) , \(receipt1.purchaseDate) , \(receipt1.subscriptionExpirationDate!) , \(receipt1.transactionId)")
                    }
                    
                    
                    SubscriptionPersistentManager.shared.setCurrentSubscription(productId: nil)
                    
                    NotificationCenter.default.post(name: .subscriptionExpired, object: nil)
                    
                case .notPurchased:
                    print("This product has never been purchased")
                    NotificationCenter.default.post(name: .subscriptionNeverPurchased, object: nil)
                    
                }
                
            case .error(let error):
                print("Verify receipt failed: \(error)")
            }
        }
    }
    
    
    override init(){
        
        var nsDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: configurationName, ofType: "plist") {
            nsDictionary = NSDictionary(contentsOfFile: path)
            
            if let monetization =  nsDictionary!["Monetization"] as? NSDictionary {
            
                if let subscription =  monetization["IAP"] as? NSDictionary {
                    self.productIdentifier = subscription["Product_ID"] as! String
                    self.status = subscription["Active"] as! Bool
                    self.title = subscription["Title"] as! String
                    self.policy = subscription["Policy"] as! String
                    self.isSubscription = subscription["Is_Subscription"] as! Bool
                    self.productDescription = subscription["description"] as! String
                    self.secretId = subscription["AppSecret_ID"] as! String
                    
                    
                    if let sliderImages =  subscription["Slider_Images"] as? NSArray {
                        self.sliderImages = sliderImages as! [String]
                        
                    }
                }
                
            }
        }
        
        SubscriptionPersistentManager.shared.registerNoOfAppLaunch()
        
        super.init()
        
    }
    
    func initialize(){
        
        SKPaymentQueue.default().add(iapObserver)
        
        
        if SubscriptionPersistentManager.shared.getCurrentSubscription() != nil {
            SubscriptionManager.shared.verifyReceipt(productId: SubscriptionManager.shared.productIdentifier)
        }
        
        
        
    }
    
    func isSubscribed()->Bool{
        return !(SubscriptionPersistentManager.shared.getCurrentSubscription() == nil)
    }
}
extension SKProduct.PeriodUnit {
    func description(capitalizeFirstLetter: Bool = false, numberOfUnits: Int? = nil) -> String {
        let period:String = {
            switch self {
            case .day: return "day"
            case .week: return "week"
            case .month: return "month"
            case .year: return "year"
            }
        }()
        
        var numUnits = ""
        var plural = ""
        if let numberOfUnits = numberOfUnits {
            numUnits = "\(numberOfUnits) " // Add space for formatting
            plural = numberOfUnits > 1 ? "s" : ""
        }
        return "\(numUnits)\(capitalizeFirstLetter ? period.capitalized : period)\(plural)"
    }
    

}

protocol  SubscriptionManagerDelegate {
    func onProductDetailsFetched(products:[SKProduct])
    func onProductDetailsFetchedError(error:Error)
}

extension Notification.Name {
    static let subscriptionPurshasing = Notification.Name("subscriptionPurshasing")
    static let subscriptionPurchased = Notification.Name("subscriptionPurchased")
    static let subscriptionExpired = Notification.Name("subscriptionExpired")
    static let subscriptionNeverPurchased = Notification.Name("subscriptionNeverPurchased")
    static let subscriptionFailed = Notification.Name("subscriptionFailed")
}


