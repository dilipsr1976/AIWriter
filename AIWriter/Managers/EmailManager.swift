//
//  documentManager.swift
//  AIdocumentAssistant
//
//  rebootcs.com
//

import Foundation
import RealmSwift


class DocumentManager {
    // Singleton instance of the DocumentManager
    static let shared = DocumentManager()
    // A private concurrent queue to perform Realm write and read operations
    private let queue = DispatchQueue(label: "trendyAppTemplates.aiwriter", qos: .userInitiated, attributes: .concurrent)
    
    private init() {}
    
    // Store a new document with the provided subject, body, and date
    func storeDocument(subject: String, body: String, date: Date) {
        // Create a new Document instance
        let document = Document()
        document.subject = subject
        document.body = body
        document.date = date
        
        // Use a concurrent queue to write to Realm
        queue.async(flags: .barrier) {
            let realm = try! Realm()
            do {
                try realm.write {
                    realm.add(document)
                }
            } catch let error {
                print("Error storing document: \(error.localizedDescription)")
            }
        }
    }
    
    // Retrieve all documents sorted by date in descending order
    func retrieveDocuments() -> Results<Document> {
        var result: Results<Document>?
        // Use a synchronous block to read from Realm
        queue.sync {
            let realm = try! Realm()
            result = realm.objects(Document.self).sorted(byKeyPath: "date", ascending: false)
        }
        return result!
    }
    
    // Delete all documents
    func deleteAllDocuments() {
        // Use a concurrent queue to write to Realm
        queue.async(flags: .barrier) {
            let realm = try! Realm()
            let documents = realm.objects(Document.self)
            do {
                try realm.write {
                    realm.delete(documents)
                }
            } catch let error {
                print("Error deleting all documents: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteDocument(document: ThreadSafeReference<Document>) {
        /**
         Deletes a document from the Realm database.
         - Parameter document: A ThreadSafeReference to the `Document` object to be deleted.
         */
        queue.async(flags: .barrier) {
            do {
                
                let realm = try! Realm()
                guard let documentRef = realm.resolve(document) else {
                    return // person was deleted
                }
                try realm.write {
                    realm.delete(documentRef)
                }
            } catch let error {
                print("Error deleting document: \(error.localizedDescription)")
            }
        }
    }
    
    func extractBody(from message: String) -> String {
        /**
         Extracts the body text from a message string.
         - Parameter message: A string representing the message.
         - Returns: A string representing the body text of the message.
         */
        let subjectPrefix = "Subject:"
        let bodyPrefix = "Body:"
        var remaining = message
        
        if let subjectRange = message.range(of: subjectPrefix, options: .caseInsensitive) {
            // Remove entire subject line, including subject text
            let newLineRange = message.range(of: "\n", range: subjectRange.upperBound..<message.endIndex)
            if let rangeToRemove = newLineRange ?? message.range(of: "\r", range: subjectRange.upperBound..<message.endIndex) {
                remaining.removeSubrange(subjectRange.lowerBound..<rangeToRemove.upperBound)
            }
        }
        
        if let bodyRange = remaining.range(of: bodyPrefix, options: .caseInsensitive) {
            // Remove "Body:" prefix
            let start = bodyRange.upperBound
            remaining.removeSubrange(remaining.startIndex..<start)
        }
        
        let trimmedResult = remaining.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedResult
    }
}
