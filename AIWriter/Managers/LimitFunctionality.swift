import Foundation

// Define keys for user defaults
struct UserDefaultsKeys {
    static let functionalityCountKey = "functionalityCountKey"
    static let lastUpdateKey = "lastUpdateKey"
}

// LimitFunctionality class to keep track of daily usage of app
class LimitFunctionality {
    static let shared = LimitFunctionality()
    
    // User defaults and calendar for date management
    private let userDefaults = UserDefaults.standard
    private let calendar = Calendar.current
    
    // Functionality count stored in user defaults
    private var functionalityCount: Int {
        get { return userDefaults.integer(forKey: UserDefaultsKeys.functionalityCountKey) }
        set { userDefaults.set(newValue, forKey: UserDefaultsKeys.functionalityCountKey) }
    }
    
    // Last update date stored in user defaults
    private var lastUpdate: Date {
        get { return userDefaults.object(forKey: UserDefaultsKeys.lastUpdateKey) as? Date ?? Date() }
        set { userDefaults.set(newValue, forKey: UserDefaultsKeys.lastUpdateKey) }
    }
    
    // Private constructor to prevent object creation
    private init() {}
    
    // Increment the usage count
    func incrementUsage() {
        let today = Date()
        if !calendar.isDateInToday(lastUpdate) {
            functionalityCount = 0
        }
        
        functionalityCount += 1
        lastUpdate = today
        
        // Show subscribe alert if daily usage limit is exceeded
        if functionalityCount > AppConfig.freeUsage {
            showSubscribeAlert()
        }
    }
    
    // Get the daily usage count
    func getDailyUsageCount() -> Int {
        return functionalityCount
    }
    
    // Get the remaining usage count for the day
    func getDailyUsageLeft() -> Int {
        return AppConfig.freeUsage - functionalityCount
    }
    
    // Check if the user has exceeded the free usage limit
    func isFreeAllowed()->Bool{
        if self.functionalityCount < AppConfig.freeUsage {
            return true
        }
        return false
    }
    
    // Private method to show a subscribe alert
    private func showSubscribeAlert(){
        
    }
}
