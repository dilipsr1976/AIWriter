//
//  WriterCell.swift
//  Ai Writer
//
//  rebootcs.com
//

import UIKit

class WriterCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var more: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
}
