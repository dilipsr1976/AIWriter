//
//  AppConfig.swift
//  Static Table View
//
//  rebootcs.com
//
import Foundation

class AppConfig {
    
    // URLs
    static let appURL = "https://apps.apple.com/us/app/6445969233" // The URL of the app in the App Store.
    static let privacyPolicy = "https://dissemino.com/Privacy-Policy" // The URL of the privacy policy.
    static let termsOfUse = "https://dissemino.com/terms-conditions" // The URL of the terms of use.
    static let shareAppText = "Check out this amazing app!"
    // Email
    static let documentSubject = "Dissemino AI Writer" // The subject of the document.
    static let workEmail = "sales@rebootcs.com" // The email address to which the email will be sent.
    
    // AdMob
    static let adMobId = "ca-app-pub-5243617045814832~9742902691" // The AdMob ID to be used in the app.
    //ca-app-pub-5243617045814832~9742902691
    // orgiginal ca-app-pub-3940256099942544/4411468910
    // OpenAI
    static let openAIToken = "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"
    //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
    // The OpenAI token to be used in the app.
    
    // Usage
    static let freeUsage = 5 // The maximum number of free uses per day for the app.
}

